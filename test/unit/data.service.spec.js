describe('dataservice', function(){

	var fakeTrello = {
		authorized: function(){},
		deauthorize: function(){},
		authorize: function(params){}
	}

	beforeEach(module('seekTrello.core', function($provide){
		$provide.value('trello', fakeTrello);
	}));

	var dataservice;
	beforeEach(inject(function(_dataservice_){
		dataservice = _dataservice_;
	}));

	describe('dataservice.rearrangeCardsIntoLists', function(){

		testresponse = {
			lists: [{
				id: 'l1'
			},
			{
				id: 'l2'
			}],
			cards: [{
				id: 'c1',
				idList: 'l1'
			},
			{
				id: 'c2',
				idList: 'l1'
			},
			{
				id: 'c3',
				idList: 'l2'
			}]
		}

		expectedData = {
			lists: [{
				id: 'l1',
				cards: [{
					id: 'c1',
					idList: 'l1'
				},
				{
					id: 'c2',
					idList: 'l1'
				}]
			},
			{
				id: 'l2',
				cards: [{
					id: 'c3',
					idList: 'l2'
				}]
			}],
			cards: [{
				id: 'c1',
				idList: 'l1'
			},
			{
				id: 'c2',
				idList: 'l1'
			},
			{
				id: 'c3',
				idList: 'l2'
			}]
		}

		it('should transform Trellos flat reponse data into a heirachy', function(){
			dataservice.__rearrangeCardsIntoLists(testresponse)
			expect(testresponse).to.deep.equal(expectedData);
		})

	})

	
});