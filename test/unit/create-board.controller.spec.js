describe('CreateBoardModalController', function(){

	var fakeTrello = {
		authorized: function(){},
		deauthorize: function(){},
		authorize: function(params){}
	}

	beforeEach(module('seekTrello.core', function($provide){
		$provide.value('trello', fakeTrello);
	}));

	beforeEach(module('seekTrello.board'));

	var fakeModalInstance = {
		close: function(value){},
		dismiss: function(){}
	}

	var fakeDataService = {
		createBoard: function(board){
			return {
				then: function(callback, error){}
			}
		}
	}

	var scope, vm, board;
	beforeEach(inject(function($rootScope, $controller){
		scope = $rootScope.$new();

		vm = $controller('CreateBoardModalController as vm', {
			$scope: scope,
			$modalInstance: fakeModalInstance, 
			dataservice: fakeDataService
		});
	}));

	it('vm should have a board, with default name', function(){
		expect(vm.board).to.have.property('name').and.equal('');
	});

	describe('CreateBoardModalController.cancel()', function(){
		it('should dismiss the $modalInstance on cancel()', function(){
			var createBoard = sinon.spy(fakeDataService, 'createBoard');
			var close = sinon.spy(fakeModalInstance, 'dismiss');

			vm.cancel();

			close.should.have.been.calledOnce;
			createBoard.should.not.have.been.called;
		})
	});

	describe('CreateBoardModalController.ok()', function(){
		it('should have the create dataservice create the board on ok()', function(){

			//todo

			vm.ok();
		})
	});
	

})