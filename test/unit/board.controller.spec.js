describe('BoardController', function(){
	beforeEach(module('seekTrello.board'));

	var scope, vm, board;
	beforeEach(inject(function($rootScope, $controller){

		board = {};
		scope = $rootScope.$new();

		vm = $controller('BoardController as vm', {
			board: board,
			$scope: scope
		});
	}));

	it('should assign board to the vm', function(){
		expect(vm.board).to.equal(board);
	});
})