describe('WelcomeController', function(){

	var fakeTrello = {
		authorized: function(){},
		deauthorize: function(){},
		authorize: function(params){}
	}

	var fakeDataservice = {
		getBoards: function(){
			return {
				then: function(callback){
					callback(board)
				}
			};
		}
	};

	var fakeAuthservice = {
		authorize: function(opts){
			return {
				then: function(callback){
					callback(true)
				}
			}
		}, 
		authorized: function(opts){
			return {
				then: function(callback){
					callback(true)
				}
			}
		}	
	}

	var board = {
		name: "testboard"
	};

	beforeEach(module('seekTrello.core', function($provide){
		$provide.value('trello', fakeTrello);
	}));

	beforeEach(module('seekTrello.welcome'));

	var scope, vm, $controller;
	beforeEach(inject(function($rootScope, _$controller_){

		scope = $rootScope.$new();

		$controller = _$controller_;
	}));

	it('should attempt to authenticate (not interactive) on creation', function(){

		var authorize = sinon.spy(fakeAuthservice, 'authorize');

		vm = $controller('WelcomeController as vm', {
			$scope: scope,
			dataservice: fakeDataservice,
			authservice: fakeAuthservice,
		});

		authorize.should.have.been.calledOnce;
		authorize.should.have.been.calledWithMatch({interactive: false});
		authorize.restore();
	});

	xit('should load the boards if authenticated on load', function(){
		
	});

	xit('isAuthorised() should call isAuthorized from the authservice', function(){
		
	});

	xit('createNewBoard should open a new modal dialog', function(){
		
	});

	xit('createNewBoard should open a new modal dialog, on success - the modal should return the board, and change the $state to the board route', function(){
		
	});

	xit('createNewBoard should open a new modal dialog, on cancel - nothing should happen', function(){
		
	});
})