describe('authservice', function(){

	var fakeTrello = {
		authorized: function(){},
		deauthorize: function(){},
		authorize: function(params){}
	}

	beforeEach(module('seekTrello.core', function($provide){
		$provide.value('trello', fakeTrello);
	}));

	var authservice;
	beforeEach(inject(function(_authservice_){
		authservice = _authservice_;
	}));


	describe('authservice.authorize', function(){
		var spy;
		beforeEach(function(){
			spy = sinon.spy(fakeTrello, 'authorize');
		})

		afterEach(function () {
			spy.restore();
		});

		it('should call trello.authorize() with default params', function(){
			authservice.authorize();

			spy.should.have.been.calledOnce;

			// ensure that interactive mode is true
			spy.should.have.been.calledWithMatch({interactive: true});
		});

		it('should call trello.authorize() with explicit params', function(){
			authservice.authorize({interactive: false});

			spy.should.have.been.calledOnce;

			// ensure that interactive mode is false
			spy.should.have.been.calledWithMatch({interactive: false});
		});
	});

	describe('authservice.authorized', function(){
		var spy;
		beforeEach(function(){
			spy = sinon.spy(fakeTrello, 'authorized');
		});

		afterEach(function () {
			spy.restore();
		});

		it('should call trello.authorized()', function(){
	 		authservice.authorized();

	 		spy.should.have.been.calledOnce;
		});
	});

	describe('authservice.isAuthorized', function(){
		var spy;
		beforeEach(function(){
			spy = sinon.spy(fakeTrello, 'deauthorize');
		});

		afterEach(function () {
			spy.restore();
		});

		it('should call trello.deauthorize()', function(){
	 		authservice.deauthorize();

	 		spy.should.have.been.calledOnce;
		});
	});
	
});