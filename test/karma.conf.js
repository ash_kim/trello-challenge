'use strict';

module.exports = function(config) {

  config.set({
    basePath : '..', //!\\ Ignored through gulp-karma //!\\

    files : [ //!\\ Ignored through gulp-karma //!\\
        'src/bower_components/angular/angular.js',
        'src/bower_components/angular/angular-route.js',
        'src/bower_components/angular-mocks/angular-mocks.js',
        'src/app/board/*.js',
        'src/app/welcome/*.js',
        'src/app/core/*.js',
        'src/app/*.js',
        'test/unit/**/*.js'
    ],

    autoWatch : true,

    colors: true,

    frameworks: ['mocha', 'chai', 'sinon', 'sinon-chai', 'chai-as-promised'],

    browsers : ['Chrome'],

    plugins : [
        'karma-phantomjs-launcher',
        'karma-firefox-launcher',
        'karma-chrome-launcher',
        'karma-mocha',
        'karma-chai',
        'karma-sinon',
        'karma-chai-plugins'
    ]
  });

};
