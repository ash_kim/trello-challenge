# Trello Challenge

Built using:

	* yeoman - with the angular-gulp generator
	* angular-bootstrap - for the modal ui component
	* trello's client.js
	* tested using mocha, sinon, and chai
	* lodash - for data manipulation
