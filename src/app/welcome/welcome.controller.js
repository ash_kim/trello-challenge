(function(){
  'use strict';


  angular.module('seekTrello.welcome')
    .controller('WelcomeController', WelcomeController);

  WelcomeController.$inject = ['dataservice', 'authservice', '$log', '$modal', '$state'];

  function WelcomeController(dataservice, authservice, $log, $modal, $state){

    var vm = this;

    vm.connect = connect;
    vm.isAuthorised = isAuthorised;
    vm.createNewBoard = createNewBoard;
    vm.boards = [];

    activate();

    function activate(){
      //attempt to login at startup:
      authservice.authorize({interactive: false}).then(function(authorized){
        if(authorized){
          dataservice.getBoards().then(getBoardsSuccess);  
        }
      }, function(error){
        $log.error(error);
      });
    }

    function connect(){
      authservice.authorize().then(function(authorized){
        if(authorized){
          dataservice.getBoards().then(getBoardsSuccess);  
        }
      }, function(error){
        $log.error(error);
      });
    }

    function isAuthorised(){
      return authservice.authorized();
    }

    function getBoardsSuccess(boards){
      vm.boards = boards;
    }

    function createNewBoard(){
      $modal.open({
        templateUrl: 'app/board/create-board.modal.html',
        controller: 'CreateBoardModalController as vm',
        size: 'sm'
      })
      .result.then(function(board){
        $state.go('board', {id: board.id});
      }); 
    }
  }
    
})();