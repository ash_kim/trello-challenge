(function(){

'use strict';

  angular.module('seekTrello.welcome', ['seekTrello.core'])
  .config(function ($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('welcome', {
        url: '/',
        templateUrl: 'app/welcome/welcome.html',
        controller: 'WelcomeController',
        controllerAs: 'vm'
      });
  });

})();