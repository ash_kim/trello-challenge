(function(){
	'use strict';

	angular.module('seekTrello.core')
	  .factory('dataservice', dataservice);

	dataservice.$inject = ['$http', '$log', '$q', 'trello'];

	function dataservice($http, $log, $q, trello){

		return {
			getBoards: getBoards,
			getBoard: getBoard,
			createBoard: createBoard,
			createCard: createCard,
			__rearrangeCardsIntoLists: rearrangeCardsIntoLists
		};

		function createBoard(board){
			var deferred = $q.defer();

			trello.post('boards', board, function(board){
				deferred.resolve(board);
			},
			function(error){
				deferred.reject('failed to create board: ' + error);
			});

			return deferred.promise;
		}

		function createCard(card){
			var deferred = $q.defer();

			trello.post('cards', card, function(card){
				deferred.resolve(card);
			},
			function(error){
				deferred.reject('failed to fetch boards: ' + error);
			});

			return deferred.promise;
		}

		function getBoards(){
			var deferred = $q.defer();

			trello.get('members/me/boards', function(boards){
				deferred.resolve(boards);
			}, 
			function(error){
				deferred.reject('failed to fetch boards: ' + error);
			});

			return deferred.promise;
		}

		// helper function to build a hierachy out of the flat card and lists 
		function rearrangeCardsIntoLists(trelloBoardData){

			var findList = _.memoize(function(listId){
				return _.find(trelloBoardData.lists, function(l){
					return l.id === listId;
				});
			});

			// initialise the cards property in each list
			_.forEach(trelloBoardData.lists, function(l){
				l.cards = [];
			});
				
			_.forEach(trelloBoardData.cards, function(c){
				var l = findList(c.idList);
				if(l){
					l.cards.push(c);
				}
					
			});

			return trelloBoardData;
		}

		function getBoard(id){
			var deferred = $q.defer();
			trello.boards.get(id, 
							  {lists: 'all', cards: 'all'}, 
							  function(board){
								deferred.resolve(rearrangeCardsIntoLists(board));
							  },
							  function(error){
							  	deferred.reject(error);
							  });
			return deferred.promise;
		}
	}

})();