(function(){
	'use strict';

	angular.module('seekTrello.core')
	  .factory('trello', trello);

	// factory for the global Trello object, so our code doesn't directly depend on it.
	function trello(){
		return Trello;
	}

})();