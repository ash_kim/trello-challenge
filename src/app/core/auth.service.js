(function(){
	'use strict';

	angular.module('seekTrello.core')
	  .factory('authservice', authservice);

	authservice.$inject = ['$log', '$q', 'trello'];


	function authservice($log, $q, trello){

		return {
			authorize: authorize,
			authorized: authorized,
			deauthorize: deauthorize
		};

		function authorized(){
			return trello.authorized();
		}

		function deauthorize(){
			trello.deauthorize();
		}

		function authorize(params){
			params = params || {};

			var deferred = $q.defer();

			_.defaults(params, {
				type: 'popup',
				name: 'seek-trello',
				persist: true,
				interactive: true,
				scope: {
					read: 'allowRead',
					write: 'allowWrite'
				},
				success: function(response){
					deferred.resolve(trello.authorized());
				},
				error: function(error){
					deferred.reject(error);
				}
			});

			trello.authorize(params);
			
			return deferred.promise;
		}
	}

})();