(function(){

'use strict';

  angular.module('seekTrello.core', 
    ['ngAnimate', 
    'ngCookies', 
    'ngTouch', 
    'ngSanitize', 
    'ui.router', 
    'ui.bootstrap'])
  .config(function ($urlRouterProvider) {
    $urlRouterProvider.otherwise('/');
  });

})();