(function(){
	'use strict';

	angular.module('seekTrello.board')
	  .directive('stCardComposer', stCardComposer)
	  .controller('CardComposerController', CardComposerController);
	  
	function stCardComposer(){
		return {
			restict: 'E',
			scope: {
				list: '='
			},
			require: '^stList',
			link: linkFn,
			controller: 'CardComposerController',
			controllerAs: 'vm',
			templateUrl: 'app/board/card-composer.html'
		};

		function linkFn($scope, $elem, $attrs, stListController){
			//bind the parent st-list directive's controller to the scope
			$scope.listController = stListController;
		}
	}

	CardComposerController.$inject = ['$scope', 'dataservice', '$log'];
	function CardComposerController($scope, dataservice, $log){

		var vm = this;

		vm.cancel = cancel;
		vm.submit = submit;
		vm.isEditing = false;
		vm.list = $scope.list;
		vm.card = {
			idList: vm.list.id
		};

		function cancel(){
			vm.isEditing = false;
		}

		function submit(){
			dataservice.createCard(vm.card).then(function(card){
				$scope.listController.add(card);
				vm.isEditing = false;
			},
			function(error){
				$log.error('failed to create card:' + error);
			});
		}
	}

})();