(function(){
'use strict';

angular.module('seekTrello.board')
  .controller('BoardController', BoardController);

BoardController.$inject = ['board'];


function BoardController(board){
	var vm = this;
	vm.board = board;
}

})();