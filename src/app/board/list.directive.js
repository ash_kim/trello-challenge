(function(){
	'use strict';

	angular.module('seekTrello.board')
	  .directive('stList', stList)
	  .controller('ListController', ListController);

	function stList(){
		return {
			restict: 'E',
			scope: {
				list: '=',
			},
			controller: ListController,
			controllerAs: 'vm',
			templateUrl: 'app/board/list.html',
			replace: true
		};
	}

	ListController.$inject = ['$scope'];
		
	function ListController($scope){

		var vm = this;

		vm.add = add;
		vm.list = $scope.list;

		function add(card){
			vm.list.cards.push(card);	
		}
	}

})();