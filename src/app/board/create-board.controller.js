(function(){
'use strict';

angular.module('seekTrello.board')
  .controller('CreateBoardModalController', CreateBoardModalController);

CreateBoardModalController.$inject = ['$modalInstance', '$log', 'dataservice'];

function CreateBoardModalController($modalInstance, $log, dataservice){
	var vm = this;

	vm.board = {
		name: ''
	};
	vm.ok = ok;
	vm.cancel = cancel;

	////////////

	function ok(){
		dataservice.createBoard(vm.board).then(function(board){
			$modalInstance.close(board);	
		},
		function(error){
			$log.error('failed to save board: ' + error);
			//TODO: deal with error 
			$modalInstance.close(null);
		});
	}

	function cancel(){
		$modalInstance.dismiss();
	}
}

})();