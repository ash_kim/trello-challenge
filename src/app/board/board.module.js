(function(){

'use strict';

  angular.module('seekTrello.board', 
    ['seekTrello.core'])
  .config(function ($stateProvider) {
    $stateProvider
      .state('board', {
        url: '/board/:id/',
        templateUrl: 'app/board/board.html',
        controller: 'BoardController',
        controllerAs: 'vm',
        resolve: {
          board: ['dataservice', '$stateParams', function(dataservice, $stateParams){
            return dataservice.getBoard($stateParams.id);
          }]
        }
      });
  });

})();