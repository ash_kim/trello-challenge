(function(){

'use strict';

angular.module('seekTrello', 
	['seekTrello.board', 
	'seekTrello.welcome', 
	'seekTrello.core']);

})();